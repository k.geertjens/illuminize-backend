plugins {
	java
	id("org.springframework.boot") version "2.7.10"
	id("io.spring.dependency-management") version "1.0.15.RELEASE"

	id("io.gatling.gradle") version "3.9.5"
	id("org.sonarqube") version "3.5.0.2730"
	id("jacoco")
}

group = "com.illuminize"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_17

configurations {
	compileOnly {
		extendsFrom(configurations.annotationProcessor.get())
	}
}

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("io.springfox:springfox-boot-starter:3.0.0")
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("org.modelmapper:modelmapper:3.1.1")

	implementation("com.cloudinary:cloudinary-core:1.33.0")
	implementation("com.cloudinary:cloudinary-http44:1.33.0")

	implementation("io.gatling:gatling-core:3.9.5")
	implementation("io.gatling:gatling-http:3.9.5")

	implementation("com.azure.spring:spring-cloud-azure-starter:4.7.0")
	implementation("com.azure:azure-security-keyvault-secrets:4.6.1")
	implementation("com.azure:azure-identity:1.8.0")

	implementation("com.microsoft.sqlserver:mssql-jdbc:11.2.3.jre17")

	implementation("org.springframework.boot:spring-boot-starter-security:2.6.3")
	implementation("org.springframework.boot:spring-boot-starter-oauth2-resource-server:3.1.0")

	runtimeOnly("com.mysql:mysql-connector-j")

	compileOnly("org.projectlombok:lombok")

	annotationProcessor("org.projectlombok:lombok")

	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("com.h2database:h2:2.1.214")
	testImplementation("com.microsoft.azure:azure-client-authentication:1.7.14")
}

tasks.withType<Test> {
	useJUnitPlatform()
	finalizedBy(tasks.jacocoTestReport)
}

tasks.jacocoTestReport {
	dependsOn(tasks.test) // tests are required to run before generating the report
	reports {
		xml.required.set(true)
		csv.required.set(true)
		html.outputLocation.set(layout.buildDirectory.dir("jacocoHtml"))
	}
}

sonar {
	properties {
		property("sonar.projectKey", "illuminize-video-api")
		property("sonar.organization", "illuminizevideoapi")
		property("sonar.qualitygate.wait", true)
		property("sonar.coverage.jacoco.xmlReportPaths", "**/build/reports/jacoco/test/jacocoTestReport.xml")
	}
}