package com.illuminize.api.models.video;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@NoArgsConstructor
@Getter @Setter
public class VideoDTO {

    public VideoDTO(int id, int poster_id, String video_url, String thumbnail_url, String title, String description){
        this.id = id;
        this.poster_id = poster_id;
        this.video_url = video_url;
        this.thumbnail_url = thumbnail_url;
        this.title = title;
        this.description = description;
    }

    private int id;
    private int poster_id;
    private LocalDateTime date_posted;
    private String video_url;
    private String thumbnail_url;
    private String title;
    private String description;
    private int views;

}
