package com.illuminize.api.models.video.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter @Setter
public class VideoPostRequest {

    private int poster_id;
    private String video_url;
    private String thumbnail_url;
    private String title;
    private String description;

}
