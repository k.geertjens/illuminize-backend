package com.illuminize.api.models.video;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@NoArgsConstructor
@Getter @Setter
public class VideoThumbnailDTO {

    private int id;
    private int poster_id;
    private LocalDateTime date_posted;
    private String thumbnail_url;
    private String title;
    private int views;

}
