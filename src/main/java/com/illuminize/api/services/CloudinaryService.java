package com.illuminize.api.services;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class CloudinaryService {

    @Value("${cloudinary-cloud-name}") private String cloudinaryCloudName;
    @Value("${cloudinary-api-key}") private String cloudinaryApiKey;
    @Value("${cloudinary-api-secret}") private String cloudinaryApiSecret;

    private Cloudinary cloudinary;

    @PostConstruct
    void init(){
        cloudinary = new Cloudinary(ObjectUtils.asMap(
                "cloud_name", cloudinaryCloudName,
                "api_key", cloudinaryApiKey,
                "api_secret", cloudinaryApiSecret,
                "secure", true
        ));
    }

    private Logger logger = Logger.getLogger("c.illuminize.api.cloudinaryservice");

    public boolean deleteVideo(UUID uuid) {
        String folderName = "illuminize/videos/" + uuid + "/";

        try{
            cloudinary.api().deleteResourcesByPrefix(folderName, ObjectUtils.asMap(
                    "resource_type", "video"
            ));
            cloudinary.api().deleteResourcesByPrefix(folderName, ObjectUtils.asMap(
                    "resource_type", "raw"
            ));
            cloudinary.api().deleteFolder(folderName, ObjectUtils.emptyMap());

            logger.log(Level.INFO,"Deleted video files in " + folderName);
            return true;
        }
        catch(Exception e){
            logger.log(Level.SEVERE,e.getMessage());
            return false;
        }
    }
}
