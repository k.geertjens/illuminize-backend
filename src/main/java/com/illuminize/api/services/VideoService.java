package com.illuminize.api.services;

import com.illuminize.api.models.video.Video;
import com.illuminize.api.models.video.VideoDTO;
import com.illuminize.api.models.video.requests.VideoPostRequest;
import com.illuminize.api.models.video.VideoThumbnailDTO;
import com.illuminize.api.models.video.requests.VideoPutRequest;
import com.illuminize.api.repositories.VideoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;
@Service
public class VideoService {

    @Autowired
    private VideoRepository videoRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private CloudinaryService cloudinaryService;

    public Iterable<VideoDTO> getAll(){
        Iterable<Video> videos = videoRepository.findAll();
        ArrayList<VideoDTO> videoDTOs = new ArrayList<VideoDTO>();

        for (Video video: videos){
            videoDTOs.add(modelMapper.map(video, VideoDTO.class));
        }

        return videoDTOs;
    }

    public VideoDTO get(int id){
        Optional<Video> video = videoRepository.findById(id);
        if(video.isEmpty()) {
            return null;
        }
        return modelMapper.map(video, VideoDTO.class);
    }

    public Iterable<VideoThumbnailDTO> getAllThumbnails(String searchString){
        Iterable<Video> videos = videoRepository.findAllBySearchString(searchString);
        ArrayList<VideoThumbnailDTO> videoThumbnailDTOs = new ArrayList<VideoThumbnailDTO>();

        for (Video video: videos){
            videoThumbnailDTOs.add(modelMapper.map(video, VideoThumbnailDTO.class));
        }

        return videoThumbnailDTOs;
    }

    public int addVideo(VideoPostRequest videoDTO){
        Video video = modelMapper.map(videoDTO, Video.class);
        video.setDate_posted(LocalDateTime.now(ZoneOffset.UTC));
        video.setViews(0);

        video = videoRepository.save(video);
        return video.getId();
    }

    public VideoDTO updateVideo(int id, VideoPutRequest videoDTO){
        Optional<Video> originalVideo = videoRepository.findById(id);
        if(originalVideo.isEmpty()) {
            return null;
        }

        Video newVideo = originalVideo.get();
        newVideo.setVideo_url(videoDTO.getVideo_url());
        newVideo.setThumbnail_url(videoDTO.getThumbnail_url());
        newVideo.setTitle(videoDTO.getTitle());
        newVideo.setDescription(videoDTO.getDescription());
        newVideo.setViews(videoDTO.getViews());

        newVideo = videoRepository.save(newVideo);
        return modelMapper.map(newVideo, VideoDTO.class);
    }

    public boolean deleteVideo(int id){
        Optional<Video> video = videoRepository.findById(id);
        if(video.isEmpty()) {
            return false;
        }

        String videoUrl = video.get().getVideo_url();
        String videoUrlTrim = videoUrl.substring(0, videoUrl.lastIndexOf("/"));
        videoUrlTrim = videoUrlTrim.substring(videoUrlTrim.lastIndexOf("/")+1, videoUrlTrim.length());

        UUID videoFileUUID = UUID.fromString(videoUrlTrim);

        boolean videoFilesDeleted = cloudinaryService.deleteVideo(videoFileUUID);
        if(videoFilesDeleted){
            videoRepository.deleteById(id);
            return true;
        }
        else{
            return false;
        }
    }
}
