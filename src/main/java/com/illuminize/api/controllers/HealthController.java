package com.illuminize.api.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/health_check")
public class HealthController {

    @GetMapping
    public ResponseEntity<String> health_check(){
        return ResponseEntity.ok().body("API is running");
    }

}
