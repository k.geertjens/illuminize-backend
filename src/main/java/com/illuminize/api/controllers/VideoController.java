package com.illuminize.api.controllers;

import com.illuminize.api.models.video.VideoDTO;
import com.illuminize.api.models.video.requests.VideoPostRequest;
import com.illuminize.api.models.video.VideoThumbnailDTO;
import com.illuminize.api.models.video.requests.VideoPutRequest;
import com.illuminize.api.services.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/videos")
public class VideoController {

    @Autowired
    private VideoService videoService;

    @GetMapping
    public ResponseEntity<Iterable<VideoDTO>> getAllVideos(){
        return ResponseEntity.ok().body(videoService.getAll());
    }

    @GetMapping("{id}")
    public ResponseEntity<VideoDTO> getVideo(@PathVariable(value="id") int id){
        VideoDTO video = videoService.get(id);
        if(video == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(video);
    }

    @GetMapping("/thumbnails")
    public ResponseEntity<Iterable<VideoThumbnailDTO>> getAllVideoThumbnails(@RequestParam(value="searchString", required = false) Optional<String> searchString){
        Iterable<VideoThumbnailDTO> thumbnails;
        if(searchString.isPresent()){
            thumbnails = videoService.getAllThumbnails(searchString.get());
        }
        else{
            thumbnails = videoService.getAllThumbnails("");
        }
        return ResponseEntity.ok().body(thumbnails);
    }

    @PostMapping
    public ResponseEntity<Integer> addVideo(@RequestBody VideoPostRequest video){
        int id = videoService.addVideo(video);
        return ResponseEntity.ok().body(id);
    }

    @PutMapping("{id}")
    public ResponseEntity updateVideo(@PathVariable(value="id") int id, @RequestBody VideoPutRequest updatedVideo){
        VideoDTO video = videoService.updateVideo(id, updatedVideo);
        if(video == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(video);
    }

    @DeleteMapping("{id}")
    public ResponseEntity deleteVideo(@PathVariable(value="id") int id){
        boolean deletedSuccessfully = videoService.deleteVideo(id);
        if (!deletedSuccessfully){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.noContent().build();
    }

}
