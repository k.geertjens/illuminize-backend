package com.illuminize.api.repositories;

import com.illuminize.api.models.video.Video;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface VideoRepositoryCustom {

    Iterable<Video> findAllBySearchString(@Param("search") String search);

}
