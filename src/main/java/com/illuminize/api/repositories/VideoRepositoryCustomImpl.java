package com.illuminize.api.repositories;

import com.illuminize.api.models.video.Video;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class VideoRepositoryCustomImpl implements VideoRepositoryCustom{

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Iterable<Video> findAllBySearchString(String search) {
        TypedQuery<Video> query = entityManager.createQuery("SELECT v FROM Video v WHERE v.title LIKE ?1 OR v.description LIKE ?1", Video.class);
        List<Video> videos = query.setParameter(1, "%" + search + "%").getResultList();
        return videos;
    }

}
