package com.illuminize.api.repositories;

import com.illuminize.api.models.video.Video;
import org.springframework.data.repository.CrudRepository;

public interface VideoRepository extends CrudRepository<Video, Integer>, VideoRepositoryCustom {
}
