package com.illuminize.api.services;

import com.illuminize.api.models.video.Video;
import com.illuminize.api.models.video.VideoDTO;
import com.illuminize.api.repositories.VideoRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class VideoServiceTests {

    @MockBean
    private VideoRepository videoRepository;

    @MockBean
    private ModelMapper modelMapper;

    @MockBean
    private CloudinaryService cloudinaryService;

    @InjectMocks
    VideoService videoService;

    static ArrayList<Video> TEST_VIDEOS = new ArrayList<Video>();

    @BeforeAll
    void initialize()
    {
        TEST_VIDEOS.add(new Video(1, 1, "video_url", "thumbnail_url", "Title", "Description"));
        TEST_VIDEOS.add(new Video(2, 1, "video_url2", "thumbnail_url2", "Title2", "Description2"));
        TEST_VIDEOS.add(new Video(3, 2, "video_url3", "thumbnail_url3", "Title3", "Description3"));

        Mockito.when(videoRepository.findAll()).thenReturn(TEST_VIDEOS);

        Mockito.when(modelMapper.map(TEST_VIDEOS.get(0), VideoDTO.class))
                .thenReturn(new VideoDTO(1, 1, "video_url", "thumbnail_url", "Title", "Description"));
        Mockito.when(modelMapper.map(TEST_VIDEOS.get(1), VideoDTO.class))
                .thenReturn(new VideoDTO(2, 1, "video_url2", "thumbnail_url2", "Title2", "Description2"));
        Mockito.when(modelMapper.map(TEST_VIDEOS.get(2), VideoDTO.class))
                .thenReturn(new VideoDTO(3, 2, "video_url3", "thumbnail_url3", "Title3", "Description3"));
    }


    @Test
    void getAllVideos() {
        ArrayList<VideoDTO> videos = (ArrayList<VideoDTO>) videoService.getAll();

        assertEquals(TEST_VIDEOS.size(), videos.size());

        int index = 0;
        for (VideoDTO video : videos) {
            assertNotNull(video);
            Video originalVideo = TEST_VIDEOS.get(index);

            assertEquals(video.getId(), originalVideo.getId());
            assertEquals(video.getPoster_id(), originalVideo.getPoster_id());
            assertEquals(video.getVideo_url(), originalVideo.getVideo_url());
            assertEquals(video.getThumbnail_url(), originalVideo.getThumbnail_url());
            assertEquals(video.getTitle(), originalVideo.getTitle());
            assertEquals(video.getDescription(), originalVideo.getDescription());

            index++;
        }
    }

}
