package load_tests

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class GatlingTest extends Simulation {

  val httpProtocol = http
    .baseUrl("https://illuminize-api.azurewebsites.net")
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

  val scn = scenario("LoadingVideos")
    .exec(http("Load all video thumbnails").get("/videos/thumbnails"))
    .pause(3)

    .exec(http("Load specific video").get("/videos/4"))
    .pause(3)

    .exec(http("Edit video").put("/videos/4")
      .headers(Map("Content-Type" -> "application/json"))
      .body(StringBody(
        """{"description": "1",
          |"thumbnail_url": "https://i.imgur.com/Gs035nD.jpeg",
          |"title": "1",
          |"views": 0,
          |"video_url": "https://res.cloudinary.com/dguxtsd45/raw/upload/v1684693084/illuminize/videos/9aa59e64-bd83-4ee7-a811-9c054b5b8d75/playlist.m3u8"}"""
          .stripMargin)))

  setUp(
    scn.inject(rampUsers(1500).during(15))
      .protocols(httpProtocol)
  )
}